/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(Vuex);
Vue.use(VueAxios, Axios);

export default new Vuex.Store({
  strict: true,
  state: {
    posts: [],
    users: [],
    photos: [],
    userTodos: [],
    userElemId: 1,
  },
  getters: {
    getUserElemId(state) {
      return state.userElemId;
    },
  },
  actions: {
    loadPosts({ commit }) {
      Axios.get('https://jsonplaceholder.typicode.com/posts')
        .then(r => r.data)
        .then((posts) => {
          commit('SET_POSTS', posts);
        });
    },
    sendPost({ commit }, newPost) {
      Axios.post('https://jsonplaceholder.typicode.com/posts', {
        title: newPost.title,
        body: newPost.content,
        userId: newPost.userId,
      })
        .then(r => r.data)
        .then((post) => {
          commit('ADD_POST', post);
        });
    },
    updatePost({ commit }, newPost) {
      Axios.put(`https://jsonplaceholder.typicode.com/posts/${newPost.id}`, {
        title: newPost.title,
        body: newPost.content,
      })
        .then(r => r.data)
        .then((post) => {
          commit('UPDATE_POST', post);
        });
    },
    loadUsers({ commit }) {
      Axios.get('https://jsonplaceholder.typicode.com/users')
        .then(r => r.data)
        .then((users) => {
          commit('SET_USERS', users);
        });
    },
    loadUserTodo({ commit, state }) {
      Axios.get(`https://jsonplaceholder.typicode.com/users/${state.userElemId}/todos`)
        .then(r => r.data)
        .then((todos) => {
          const list = todos.filter(todo => todo.userId === state.userElemId);
          commit('SET_USER_TODOS', list);
        });
    },
    setUserElemID({ commit }, id) {
      commit('SET_USER_ELEM_ID', id);
    },
    loadPhotos({ commit }, album) {
      Axios.get(`https://jsonplaceholder.typicode.com/albums/${album}/photos`)
        .then(r => r.data)
        .then((photos) => {
          const list = photos.slice(4949);
          console.log(list);
          commit('SET_PHOTOS', list);
        });
    },
  },
  mutations: {
    SET_POSTS(state, posts) {
      state.posts = posts;
    },
    ADD_POST(state, post) {
      state.posts.unshift(post);
    },
    UPDATE_POST(state, post) {
      const index = state.posts.findIndex(element => element.id === post.id);
      state.posts[index].title = post.title;
      state.posts[index].body = post.body;
    },
    SET_USERS(state, users) {
      state.users = users;
    },
    SET_USER_TODOS(state, todos) {
      state.userTodos = todos;
    },
    SET_USER_ELEM_ID(state, id) {
      state.userElemId = id;
    },
    SET_PHOTOS(state, photos) {
      state.photos = photos;
    },
  },
});
